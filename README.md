# GitLab-container-registry
![Diagrama Dockerfiles](./Dockerfile_Diagram.drawio.png)

## Getting started

Este repositorio contiene una colección de imágenes de Docker listas para usar para diferentes aplicaciones. Las imágenes de Docker son una forma eficiente y portátil de empacar y distribuir aplicaciones y sus dependencias.

## Name
GitLab Container registry

## Description
Crear y guarda imagenes Docker en tu proyecto 

Este repositorio contiene una colección de imágenes de Docker listas para usar para diferentes aplicaciones.

Las imagenes fueron creadas para solventar mis requerimiento en los pipelines, así como reducir el tiempo de ejecución de los jobs y conocer más de container registry 

## Contenido

En este repositorio, encontrarás las siguientes imágenes de Docker:

- **Dockerfile.awscli.terraform**: Imagen con AWS CLI y Terraform.
- **Imagen 2**: Descripción de la imagen 2 y su uso.
- ...

## Previous requirements

Asegúrate de tener instalado Docker en tu sistema antes de usar estas imágenes. Puedes descargar e instalar Docker desde [https://www.docker.com/](https://www.docker.com/).

## Usage

Para usar estas imágenes de Docker, sigue estos pasos:

1. Clona este repositorio en tu máquina local:

   ```bash
   git clone git@gitlab.com:public-projects8543539/gitlab-container-registry.git
   ```

1. Construye la imagen Docker que necesitas. Por ejemplo, para construir la "Dockerfile.awscli.terraform", ejecuta:
  
   ```bash
   docker build -t nombre-de-la-imagen:etiqueta -f Dockerfile.awscli.terraform .
   ```

### Project 

Forma de ocupar la imagen de nuestro registry en un job de otro proyecto 

```bash
   build-job:       # This job runs in the build stage, which runs first.
   stage: build
   image: registry.gitlab.com/public-projects8543539/gitlab-container-registry/awsterraform:latest
   script:
      - aws --version
      - terraform --version
```

## Contributing
¡Las contribuciones son bienvenidas! Si deseas mejorar alguna imagen de Docker o agregar nuevas imágenes, sigue estos pasos:

Haz un fork de este repositorio.
Realiza tus cambios en tu fork.
Envía un pull request a este repositorio.


## Authors and acknowledgment
Este proyecto fue creado y es mantenido por Aquiles Lázaro.

- Nombre: Aquiles Lázaro
- Correo Electrónico: aquileslazaroh@gmail.com
- Perfil de GitLab: [GitLab](https://gitlab.com/aquileslh)
- Perfil de GitHub: [GitHub](https://github.com/aquileslh)

## License
Este proyecto está bajo la [Licencia MIT](LICENSE).

La Licencia MIT es una licencia de código abierto muy permisiva que permite a otros utilizar, modificar y redistribuir tu código, ya sea en proyectos comerciales o no comerciales, siempre que se incluya el aviso de copyright y la licencia original.

Puedes encontrar más detalles en el archivo [LICENSE](LICENSE) o en la [página de la Licencia MIT](https://opensource.org/licenses/MIT).
